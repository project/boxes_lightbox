<?php

/**
 * A box to show a link to lightbox content.
 */
class lightbox_box extends boxes_box {
  /**
   * Implementation of boxes_box::options_defaults().
   */
  public function options_defaults() {
   return array(
     'title' => '',
     'additional_classes' => '',
     'lightbox_type' => '',
     'width' => 300,
     'height' => 300,
     'caption' => '',
     'url' => '',
     'link_text' => '',
   );
  }

  /**
   * Define the types of lightbox we support.
   *
   * Just the one for now: please post patches for more ;)
   */
  function lightbox_types() {
    return array(
      'webpage' => t('Web page'),
    );
  }

  /**
   * Implementation of boxes_box::options_form().
   */
  public function options_form() {
    $form = array();

    $form['lightbox_type'] = array(
      '#type' => 'radios',
      '#title' => t('Lightbox type'),
      '#required' => TRUE,
      '#options' => $this->lightbox_types(),
      '#default_value' => $this->options['lightbox_type'],
      '#description' => t('The type of lightbox content to show.'),
    );

    $form['width'] = array(
      '#type' => 'textfield',
      '#title' => t('Lightbox width'),
      '#size' => 10,
      '#default_value' => $this->options['width'],
      '#description' => t('The width of the lightbox.'),
    );

    $form['height'] = array(
      '#type' => 'textfield',
      '#title' => t('Lightbox height'),
      '#size' => 10,
      '#default_value' => $this->options['height'],
      '#description' => t('The height of the lightbox.'),
    );

    $form['caption'] = array(
      '#type' => 'textfield',
      '#title' => t('Lightbox caption'),
      '#default_value' => $this->options['caption'],
      '#description' => t('The caption in the lightbox.'),
    );

    // @todo: when we have more types, these should be dependent elements
    // using CTools form dependents.
    $form['webpage']['url'] = array(
      '#type' => 'textfield',
      '#title' => t('Web page URL'),
      '#default_value' => $this->options['url'],
      '#description' => t('The URL or path to display.'),
    );

    $form['webpage']['link_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Link text'),
      '#default_value' => $this->options['link_text'],
      '#description' => t('The text to link.'),
    );

    return $form;
  }

  /**
   * Implementation of boxes_box::render().
   */
  public function render() {
    // Get the options.
    $caption  = $this->options['caption'];
    $width    = $this->options['width'];
    $height   = $this->options['height'];

    // Make the link.
    $content = l(
      $this->options['link_text'],
      $this->options['url'],
      array(
        'attributes' => array(
          'rel' => "lightframe[|width:{$width}px; height:{$height}px; scrolling: auto;][$caption]",
        ),
      ));

    $title = isset($this->title) ? check_plain($this->title) : NULL;
    return array(
      'delta' => $this->delta, // Crucial.
      'title' => $title,
      'subject' => $title,
      'content' => $content,
    );
  }
}
